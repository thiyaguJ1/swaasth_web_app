/*================================================================
App swaasthWebApp
==================================================================*/
'use strict';

var app = angular.module('swaasthWebApp', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    	.when('', {
            controller: '',
            templateUrl: ''
    	})

    	.otherwise({ redirectTo: '/' });
}]);

